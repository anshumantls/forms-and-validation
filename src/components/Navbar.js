import React from 'react';
import { NavLink } from 'react-router-dom'

import Logo from '../assets/logo.png';

const navbar = () => {
    let style = {
        color: 'green'
    }
    return (
        <nav className='navbar'>
            <div className='logo'><img alt='logo' src={Logo}/></div>
            <div></div>
            <div>
                <ul className='navbarul'>
                    <li><NavLink exact activeStyle={style} to='/edit-doctor-info'>Edit doctor details</NavLink></li>
                    <li><NavLink activeStyle={style} to='/edit-doctor-timing'>Edit doctor timing</NavLink></li>
                </ul>
            </div>
        </nav>
    );
}

export default navbar;