import React, { Component } from 'react';
import './App.css';
import { Switch, Route, Redirect } from 'react-router-dom';

//component
import Navbar from './components/Navbar';
import EditDoctorDetails from './components/Edit-Doctor-Details';
import EditDoctorTiming from './components/Edit-Doctor-Timing';

class App extends Component {
  render () {
    return (
      <div className="App">
        <Navbar/>
        <Switch>
          <Route path='/edit-doctor-info' component={EditDoctorDetails}/>
          <Route path='/edit-doctor-timing' component={EditDoctorTiming}/>
          <Redirect from='/' to='/edit-doctor-info'/>
        </Switch>
      </div>
    );
  }
}

export default App;
